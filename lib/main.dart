import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertemplate/blocs/counter/counter_bloc.dart';
import 'package:fluttertemplate/pages/counter_page.dart';
import 'package:fluttertemplate/pages/home_page.dart';
import 'package:fluttertemplate/pages/login_page.dart';
import 'package:fluttertemplate/pages/register_page.dart';
import 'package:fluttertemplate/simple_bloc_delegate.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CounterBloc>(
          create: (context) => CounterBloc(),
        ),
      ],
      child: MaterialApp(
        home: LoginPage(),
        initialRoute: LoginPage.id,
        routes: {
          LoginPage.id: (context) => LoginPage(),
          RegisterPage.id: (context) => RegisterPage(),
          HomePage.id: (context) => HomePage()
        },
      ),
    );
  }
}

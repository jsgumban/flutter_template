import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertemplate/components/on_boarding_footer.dart';
import 'package:fluttertemplate/components/on_boarding_header.dart';
import 'package:fluttertemplate/components/rounded_button.dart';
import 'package:fluttertemplate/components/rounded_text_field.dart';
import 'package:fluttertemplate/pages/home_page.dart';
import 'package:fluttertemplate/pages/register_page.dart';
import 'package:fluttertemplate/components/contants.dart';

class LoginPage extends StatefulWidget {
  static const String id = 'login_page';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _auth = FirebaseAuth.instance;
  String email = 'leo@gmail.com';
  String password = 'password';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: kPagePadding,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              OnBoardingHeader(
                headerText: 'Welcome',
                subHeaderText: 'Sign in to continue',
              ),
              Column(
                children: <Widget>[
                  RoundedTextField(
                    label: 'Email',
                    type: TextInputType.emailAddress,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RoundedTextField(
                    label: 'Password',
                    isObscure: true,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      'Forgot password?',
                      style: kForgotPasswordTextStyle,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  RoundedButton(
                    label: 'Login',
                    onPressed: () async {
                      try {
                        final user = await _auth.signInWithEmailAndPassword(
                            email: email, password: password);

                        if (user != null) {
                          Navigator.pushNamed(context, HomePage.id);
                        }
                      } catch (e) {
                        print(e);
                      }
                    },
                  ),
                ],
              ),
              OnBoardingFooter(
                text: 'I am a new user. ',
                action: 'Register',
                onTap: () {
                  Navigator.pushNamed(context, RegisterPage.id);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

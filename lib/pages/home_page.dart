import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertemplate/pages/contacts_page.dart';
import 'package:fluttertemplate/pages/messages_page.dart';
import 'package:fluttertemplate/pages/profile_page.dart';

class HomePage extends StatefulWidget {
  static const String id = 'home_page';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var page = 1;
  List<Widget> pages = [
    ContactsPage(),
    MessagesPage(),
    ProfilePage(),
  ];

  List<String> appBarTitle = [
    'Contacts',
    'Messages',
    'Profile',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle[page]),
      ),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: pages[page],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.white,
        color: Colors.lightBlueAccent,
        buttonBackgroundColor: Colors.white,
        index: page,
        items: <Widget>[
          Icon(Icons.list, size: 30),
          Icon(Icons.message, size: 30),
          Icon(Icons.account_circle, size: 30),
        ],
        onTap: (value) {
          setState(() {
            page = value;
          });
        },
      ),
    );
  }
}

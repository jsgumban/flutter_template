import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertemplate/components/on_boarding_footer.dart';
import 'package:fluttertemplate/components/on_boarding_header.dart';
import 'package:fluttertemplate/components/rounded_button.dart';
import 'package:fluttertemplate/components/rounded_text_field.dart';
import 'package:fluttertemplate/components/contants.dart';
import 'package:fluttertemplate/pages/login_page.dart';
import 'package:fluttertemplate/pages/home_page.dart';

class RegisterPage extends StatefulWidget {
  static const String id = 'register_page';

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _auth = FirebaseAuth.instance;
  String email = 'scorpio@gmail.com';
  String password = 'password';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: kPagePadding,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              OnBoardingHeader(
                headerText: 'Create account',
                subHeaderText: 'Sign up to get started',
              ),
              Column(
                children: <Widget>[
                  RoundedTextField(
                    label: 'Name',
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RoundedTextField(
                    label: 'Email',
                    type: TextInputType.emailAddress,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RoundedTextField(
                    label: 'Password',
                    isObscure: true,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  RoundedButton(
                    label: 'Register',
                    onPressed: () async {
                      try {
                        final newUser =
                            await _auth.createUserWithEmailAndPassword(
                                email: email, password: password);
                        if (newUser != null) {
                          Navigator.pushNamed(context, HomePage.id);
                        }
                      } catch (e) {
                        print(e);
                      }
                    },
                  ),
                ],
              ),
              OnBoardingFooter(
                text: 'Already have an account. ',
                action: 'Log in',
                onTap: () {
                  Navigator.pushNamed(context, LoginPage.id);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

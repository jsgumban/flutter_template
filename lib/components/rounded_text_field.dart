import 'package:flutter/material.dart';

class RoundedTextField extends StatelessWidget {
  RoundedTextField({@required this.label, this.type, this.isObscure});
  final String label;
  final TextInputType type;
  final bool isObscure;

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: type,
      obscureText: isObscure ?? false,
      decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(
          color: Colors.grey[400],
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: Colors.grey[300],
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}

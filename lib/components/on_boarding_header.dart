import 'package:flutter/material.dart';

class OnBoardingHeader extends StatelessWidget {
  OnBoardingHeader({@required this.headerText, @required this.subHeaderText});

  final String headerText;
  final String subHeaderText;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 50,
        ),
        Text(
          headerText,
          style: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          subHeaderText,
          style: TextStyle(
            fontSize: 20,
            color: Colors.grey[400],
          ),
        ),
      ],
    );
  }
}

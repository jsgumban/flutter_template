part of 'counter_bloc.dart';

@immutable
abstract class CounterEvent extends Equatable {
  final int hello;
  const CounterEvent({this.hello});
}

class IncrementEvent extends CounterEvent {
  final int hello;
  IncrementEvent({this.hello});

  @override
  List<Object> get props => [hello];

  @override
  String toString() {
    return props.toString();
  }
}

class DecrementEvent extends CounterEvent {
  @override
  List<Object> get props => [];
}

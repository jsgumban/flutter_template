part of 'counter_bloc.dart';

@immutable
abstract class CounterState extends Equatable {
  final int counter;
  CounterState({this.counter});
}

class CounterStateLoading extends CounterState {
  final int counter;
  CounterStateLoading({this.counter});

  @override
  List<Object> get props => [counter];
}

class CounterLoaded extends CounterState {
  final int counter;
  CounterLoaded({this.counter});

  @override
  List<Object> get props => [counter];

  @override
  String toString() {
    return props.toString();
  }
}
